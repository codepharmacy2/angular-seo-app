import { Component, OnInit, OnDestroy } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit, OnDestroy {

  constructor(private meta: Meta, title: Title) {
    title.setTitle('Angular - Contact');
        meta.addTags([
          { property: 'og:title',   content: 'Code Pharmacy - Contact Page'},
          { property: 'og:description', content: 'This is Contact page of our SEO Friendly Angular App'},
          { property: 'og:url', content: 'http://codepharmacy.com/contact' },
          // tslint:disable-next-line:max-line-length
          { property: 'og:image', content: 'http://i0.kym-cdn.com/entries/icons/original/000/008/610/deathnote-1280-1490294885083_1280w.jpg' }
        ]);
      }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.meta.removeTag('property="og:title"');
    this.meta.removeTag('property="og:description"');
    this.meta.removeTag('property="og:url"');
    this.meta.removeTag('property="og:image"');
  }
}
