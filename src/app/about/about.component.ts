import { Component, OnInit, OnDestroy } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit, OnDestroy{

  constructor(private meta: Meta, title: Title) {
    title.setTitle('Angular - About');
        meta.addTags([
          { property: 'og:title',   content: 'Code Pharmacy - About Page'},
          { property: 'og:description', content: 'This is About page of our SEO Friendly Angular App'},
          { property: 'og:url', content: 'http://codepharmacy.com/about' },
          { property: 'og:image', content: 'http://www.behindthevoiceactors.com/_img/shows/banner_3540.jpg' }
        ]);
      }


  ngOnInit() {
  }
  ngOnDestroy() {
    this.meta.removeTag('property="og:title"');
    this.meta.removeTag('property="og:description"');
    this.meta.removeTag('property="og:url"');
    this.meta.removeTag('property="og:image"');
  }
}
