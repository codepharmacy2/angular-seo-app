import { Component, OnInit, OnDestroy } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  constructor(private meta: Meta, title: Title) {
    title.setTitle('Angular - Home');
        meta.addTags([
          { property: 'og:title',   content: 'Code Pharmacy - Home Page'},
          { property: 'og:description', content: 'This is home page of our SEO Friendly Angular App'},
          { property: 'og:url', content: 'http://codepharmacy.com' },
          { property: 'og:image', content: 'http://blablaster.com/wp-content/uploads/2017/05/fo.jpg' }
        ]);
      }

      ngOnInit() {
  }

  ngOnDestroy() {
    this.meta.removeTag('property="og:title"');
    this.meta.removeTag('property="og:description"');
    this.meta.removeTag('property="og:url"');
    this.meta.removeTag('property="og:image"');
  }
}
